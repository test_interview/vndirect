import 'package:flutter/material.dart';
import 'package:vndirect/extension/color_extensions.dart';
import 'package:vndirect/widgets/chop_asset_widget/chop_asset_widget.dart';
import 'package:vndirect/widgets/community_widget/community_widget.dart';
import 'package:vndirect/widgets/consulting_services.dart';
import 'package:vndirect/widgets/knowledge_base_widget/knowledge_base_widget.dart';
import 'package:vndirect/widgets/mission_widget.dart';
import 'package:vndirect/widgets/title_widget/titile_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({
    super.key,
  });

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                color: HexColor.fromHex('#745BEF'), child: const TitlePage()),
            const ChopAssetWidget(),
            const SizedBox(height: 18,),
            const KnowLedgeBaseWidget(),
            const SizedBox(height: 28,),
            const MissionWidget(),
            const SizedBox(height: 28,),
            const ConsultingServices(),
            const SizedBox(height: 28,),
            const CommunityWidget(),
            const SizedBox(height: 50,),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
