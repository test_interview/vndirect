/// Package import
// import 'dart:html';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

/// Chart import
import 'package:syncfusion_flutter_charts/charts.dart';

class SplineArea extends StatefulWidget {
  const SplineArea({super.key});

  @override
  State<SplineArea> createState() => _SplineAreaState();
}

class _SplineAreaState extends State<SplineArea> {
  _SplineAreaState();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 10,),
        _buildSplineAreaChart(),
      ],
    );
  }

  /// Returns the cartesian spline are chart.
  SfCartesianChart _buildSplineAreaChart() {
    return SfCartesianChart(
      legend: const Legend(isVisible: false, opacity: 0.7),
      primaryXAxis: DateTimeAxis(
        majorGridLines: const MajorGridLines(width: 1,dashArray: [0,10]),
        majorTickLines: const MajorTickLines(size: 0),
        dateFormat: DateFormat('MM/yyyy'),
      ),
      primaryYAxis: NumericAxis(
          labelFormat: '{value}%',
          majorGridLines: const MajorGridLines(width: 0),
          majorTickLines: const MajorTickLines(size: 0),
        labelStyle: TextStyle(color: Colors.red),
          ),
      series: _getSplieAreaSeries(),
      tooltipBehavior: TooltipBehavior(enable: true,canShowMarker: true),
    );
  }

  List<_SplineAreaData>? chartData;

  @override
  void initState() {
    chartData = <_SplineAreaData>[
      _SplineAreaData(DateTime(2023, 10), -1.53, 3.3),
      _SplineAreaData(DateTime(2023, 8), 1.5, 5.4),
      _SplineAreaData(DateTime(2023, 7), 0, 2.65),
      _SplineAreaData(DateTime(2023, 5), 1.4, 2.62),
      _SplineAreaData(DateTime(2023, 4), 1.8, 1.99),
      _SplineAreaData(DateTime(2023, 3), -4.9, 1.44),
      _SplineAreaData(DateTime(2023, 2), -4.9, 1.44),
    ];
    // chartData = <_SplineAreaData>[
    //   _SplineAreaData(2023, -1.53, 3.3),
    //   _SplineAreaData(2022, 1.5, 5.4),
    //   _SplineAreaData(2021, 0, 2.65),
    //   _SplineAreaData(2020, 1.4, 2.62),
    // ];
    super.initState();
  }

  /// Returns the list of chart series
  /// which need to render on the spline area chart.
  List<ChartSeries<_SplineAreaData, DateTime>> _getSplieAreaSeries() {
    return <ChartSeries<_SplineAreaData, DateTime>>[
      SplineAreaSeries<_SplineAreaData, DateTime>(
        dataSource: chartData!,
        color: const Color.fromRGBO(75, 135, 185, 0.6),
        borderColor: const Color.fromRGBO(75, 135, 185, 1),
        borderWidth: 1,
        xValueMapper: (_SplineAreaData sales, _) => sales.year,
        yValueMapper: (_SplineAreaData sales, _) => sales.y1,
        name: 'data'
        // dataLabelSettings:  _buildDataLabelSetting(),
        // dataLabelMapper:  (_SplineAreaData sales, _) => '${sales.year.toString().substring(0, 2)} - 12'
      ),
      // SplineAreaSeries<_SplineAreaData, double>(
      //   dataSource: chartData!,
      //   borderColor: const Color.fromRGBO(192, 108, 132, 1),
      //   color: const Color.fromRGBO(192, 108, 132, 0.6),
      //   borderWidth: 2,
      //   xValueMapper: (_SplineAreaData sales, _) => sales.year,
      //   yValueMapper: (_SplineAreaData sales, _) => sales.y2,
      //     dataLabelMapper:  (_SplineAreaData sales, _) => sales.year.toString().substring(0,2)
      // )
    ];
  }

  @override
  void dispose() {
    chartData!.clear();
    super.dispose();
  }

  DataLabelSettings? _buildDataLabelSetting(){
    return DataLabelSettings(
        isVisible: true,
        alignment: ChartAlignment.far,
        showZeroValue: false,
        builder: (dynamic data, dynamic point, dynamic series, int pointIndex,
            int seriesIndex) {
          double max = 123.1;

          double min = 1.1;

          print('max chart: ${max.toString()}');
          print('min chart: ${min.toString()}');
          if (data['y1'] == max) {
            // Given width and height for the container
            return Container(
              width: 30,
              height: 30,
              child:Text(
                data['y1'].toString(),
                overflow: TextOverflow.clip,
                style: TextStyle(
                  fontSize: 10.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            );
          } else if (data['y1'] == min) {
            // Given width and height for the container
            return Container(
              width: 30,
              height: 30,
              child:Text(
                data['y1'].toString(),
                style: TextStyle(fontSize: 10.0, fontWeight: FontWeight.bold),
              ),
            );
          }

          return SizedBox();
        });
  }
}



/// Private class for storing the spline area chart datapoints.
class _SplineAreaData {
  _SplineAreaData(this.year, this.y1, this.y2);

  final DateTime year;
  final double y1;
  final double y2;
}

extension DatetimeEx on DateTime {
  String get format {
    return DateFormat('MM/yyyy').format(this);
  }
}
