import 'package:flutter/material.dart';
import 'package:vndirect/extension/color_extensions.dart';
import 'package:vndirect/generated/assets.dart';
import 'package:vndirect/widgets/component_widget/title_content_widget.dart';


class MissionWidget extends StatelessWidget {
  const MissionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _title(),
          const SizedBox(height: 12,),
          SizedBox(
            height: 70,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: 2,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return TitleContentWidget(
                    colorBg: HexColor.fromHex('EFECFF'),
                    colorCi: HexColor.fromHex('CEC5F9'),
                    url: Assets.assetsHappy,
                    title: 'Hoàn thiện hồ sơ mở tài khoản',
                    height: 70,
                    width: 254,
                  );
                }
            ),
          )

        ],
      ),
    );
  }

  _title(){
    return  GestureDetector(
      onTap: (){
        print('======= load more');
      },
      child: Row(
        children: [
          Expanded(
            child: Text('Nhiệm vụ hôm nay', style: TextStyle(
                fontSize: 20,
                color: HexColor.fromHex('1F2124'),
                fontWeight: FontWeight.w600
            ),),
          ),
          Text('Xem thêm', style: TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w600,
            color: HexColor.fromHex('745BEF'),
          ),)
        ],
      ),
    );
  }
}
