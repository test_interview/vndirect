import 'package:flutter/material.dart';
import 'package:vndirect/extension/color_extensions.dart';
import 'package:vndirect/generated/assets.dart';

class TitleContentWidget extends StatelessWidget {
  final Color colorBg;
  final Color colorCi;
  final double? height;
  final double? width;
  final String url;
  final String title;
  final String? content;

  const TitleContentWidget(
      {Key? key,
      required this.colorBg,
      required this.colorCi,
      this.height,
      required this.url,
      required this.title,
      this.width,
      this.content})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      margin: const EdgeInsets.only(right: 12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: colorBg,
      ),
      child: Stack(
        children: [
          Positioned(
            bottom: 0,
            left: 0,
            child: ClipRRect(
                borderRadius:
                    const BorderRadius.only(bottomLeft: Radius.circular(16)),
                child: Image.asset(
                  Assets.assetsYellowBottom,
                  color: colorCi,
                )),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Row(
              children: [
                Image.asset(url),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: content != null
                      ? Text(
                          title,
                          maxLines: 2,
                          style: TextStyle(
                            fontSize: 16,
                            color: HexColor.fromHex('2E3034'),
                            fontWeight: FontWeight.w600,
                          ),
                        )
                      : Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              title,
                              maxLines: 1,
                              style: TextStyle(
                                fontSize: 16,
                                color: HexColor.fromHex('2E3034'),
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                            Text(
                              content ?? '',
                              maxLines: 1,
                              style: TextStyle(
                                fontSize: 12,
                                color: HexColor.fromHex('5A5F67'),
                                fontWeight: FontWeight.w400,
                              ),
                            )
                          ],
                        ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
