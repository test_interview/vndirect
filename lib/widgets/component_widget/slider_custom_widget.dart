import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class SliderCustom extends StatefulWidget {
  final List<String> images;
  final BorderRadius? borderRadius;
  final Function(int index)? onIndexTap;
  final EdgeInsetsGeometry? padding;

  /// customize dot
  final double? spacing;
  final double? radius;
  final double? dotWidth;
  final double? dotHeight;
  final double? strokeWidth;
  final PaintingStyle? paintingStyle;
  final Color? dotColor;
  final Color? activeDotColor;

  const SliderCustom({
    Key? key,
    required this.images,
    this.borderRadius,
    this.onIndexTap,
    this.padding,
    this.spacing,
    this.radius,
    this.dotWidth,
    this.dotHeight,
    this.strokeWidth,
    this.paintingStyle,
    this.dotColor,
    this.activeDotColor,
  }) : super(key: key);

  @override
  _SliderCustomState createState() => _SliderCustomState();
}

class _SliderCustomState extends State<SliderCustom> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {

    return Container(
      padding: widget.padding,
      child: Column(
        children: [
          CarouselSlider.builder(
            itemCount: widget.images.length,
            options: CarouselOptions(
              autoPlay: true,
              enableInfiniteScroll: true,
              disableCenter: false,
              enlargeCenterPage: true,
              aspectRatio: 480 / 160,
              autoPlayAnimationDuration: const Duration(milliseconds: 300),
              autoPlayInterval: const Duration(milliseconds: 3000),
              pageSnapping: true,
              viewportFraction: 1.0,
              onPageChanged: (index, reason) {
                _currentIndex = index;
                setState(() {});
              },
            ),
            itemBuilder: (
                context,
                index,
                realIndex,
                ) {
              return GestureDetector(
                onTap: () {
                  if (widget.onIndexTap != null) {
                    widget.onIndexTap!(index);
                  }
                },
                behavior: HitTestBehavior.opaque,
                child: Stack(
                  children: [
                    ClipRRect(
                      borderRadius: widget.borderRadius ??
                          const BorderRadius.all(Radius.circular(14)),
                      child: Image.asset(
                        widget.images[index],
                        fit: BoxFit.fill,
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
          AnimatedSmoothIndicator(
            count: widget.images.length,
            activeIndex: _currentIndex,
            effect: SlideEffect(
              spacing: widget.spacing ?? 6.0,
              radius: widget.radius ?? 3.0,
              dotWidth: widget.dotWidth ?? 15.0,
              dotHeight: widget.dotHeight ?? 5.0,
              paintStyle: widget.paintingStyle ?? PaintingStyle.stroke,
              strokeWidth: widget.strokeWidth ?? 1,
              dotColor: widget.dotColor ?? Colors.red,
              activeDotColor: widget.activeDotColor ?? Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}