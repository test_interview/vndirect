import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vndirect/extension/color_extensions.dart';
import 'package:vndirect/generated/assets.dart';

class ItemInverterWidget extends StatelessWidget {
  final List<String> listItem;
  final String asset;
  final String title;
  final String content;

  const ItemInverterWidget(
      {Key? key,
      required this.listItem,
      required this.asset,
      required this.title,
      required this.content})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 254,
      height: 74,
      margin: const EdgeInsets.only(right: 12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 3,
            blurRadius: 4,
            offset: Offset(0, 2), // changes position of shadow
          ),
        ],
      ),
      child: Stack(
        children: [
          Positioned(
            bottom: 0,
            left: 0,
            child: ClipRRect(
                borderRadius:
                    const BorderRadius.only(bottomLeft: Radius.circular(16)),
                child: Image.asset(Assets.assetsYellowBottom)),
          ),
          Positioned(
              right: 10,
              top: 10,
              child: SizedBox(
                height: 16,
                child: ListView.builder(
                    padding: const EdgeInsets.all(0),
                    scrollDirection: Axis.horizontal,
                    itemCount: listItem.length,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      return SvgPicture.asset(listItem[index]);
                    }),
              )),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
            child: Row(
              children: [
                Image.asset(asset),
                const SizedBox(
                  width: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      maxLines: 1,
                      style: TextStyle(
                        fontSize: 14,
                        color: HexColor.fromHex('353A40'),
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Text(
                      content,
                      maxLines: 1,
                      style: TextStyle(
                        fontSize: 12,
                        color: HexColor.fromHex('4B5056'),
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
