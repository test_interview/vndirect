import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vndirect/extension/color_extensions.dart';
import 'package:vndirect/generated/assets.dart';

enum TypeInvestment {investmentGrowth, investInAssets, defensiveFinance}

class TitleInvestmentWidget extends StatelessWidget {
  final TypeInvestment investment;
  final String balance;

  const TitleInvestmentWidget({Key? key, required this.investment, required this.balance}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String title = '';
    String urlAsset = '';
    switch(investment){
      case TypeInvestment.investmentGrowth:
        title = 'Đầu tư tăng trưởng';
        urlAsset = Assets.assetsTitle1;
        break ;
      case TypeInvestment.investInAssets:
        title = 'Đầu tư tích sản';
        urlAsset = Assets.assetsTitle2;
        break;
      case TypeInvestment.defensiveFinance:
        title = 'Tài chính phòng vệ';
        urlAsset = Assets.assetsTitle3;
        break;
    }

    return SizedBox(
      height: 44,
      child: Stack(
        children: [
          Image.asset(
            urlAsset, // Replace with your SVG image path
            fit: BoxFit.fitHeight, // Adjust to fit your needs
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 7,vertical: 4),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title, style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: HexColor.fromHex('1F2124'),
                ),),
                Row(
                  children: [
                    SvgPicture.asset(
                      Assets.assetsCoin, // Replace with your SVG image path
                      fit: BoxFit.fitHeight, // Adjust to fit your needs
                    ),
                     const SizedBox(width: 10,),
                    Expanded(child: Text('$balanceđ',style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: HexColor.fromHex('1F2124'),
                    ),))
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
