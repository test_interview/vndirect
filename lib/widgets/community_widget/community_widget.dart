import 'package:flutter/material.dart';
import 'package:vndirect/extension/color_extensions.dart';
import 'package:vndirect/generated/assets.dart';
import 'package:vndirect/widgets/community_widget/comment_widget.dart';

class CommunityWidget extends StatelessWidget {

  const CommunityWidget({Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          _title(),
          const SizedBox(
            height: 12,
          ),
          ListView.builder(
              padding: const EdgeInsets.all(0),
              physics: const NeverScrollableScrollPhysics(),
              itemCount: 2,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return const CommentWidget(
                    urlAvatar: Assets.assetsAvatarComment,
                    name: 'Lê Trọng Hiếu',
                    time: '15 phút',
                    title: 'DGC – Thiên thời, địa lợi – Báo cáo lần đầu',
                    comment: 'Là công ty số 1 Việt Nam về công suất và công nghệ trong ngành sản xuất photpho, với 48% thị phần Là công ty số 1 Việt Nam về công suất và công nghệ trong n Là công ty số 1 Việt Nam về công suất và công nghệ trong n');
              }
          ),
        ],
      ),
    );
  }

  _title() {
    return GestureDetector(
      onTap: () {
        print('======= community');
      },
      child: Row(
        children: [
          Expanded(
            child: Text(
              'Cộng đồng',
              style: TextStyle(
                  fontSize: 20,
                  color: HexColor.fromHex('1F2124'),
                  fontWeight: FontWeight.w600),
            ),
          ),
          Text(
            'Xem thêm',
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w600,
              color: HexColor.fromHex('745BEF'),
            ),
          )
        ],
      ),
    );
  }
}
