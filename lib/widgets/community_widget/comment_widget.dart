import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:vndirect/extension/color_extensions.dart';
import 'package:vndirect/generated/assets.dart';

class CommentWidget extends StatelessWidget {
  final String urlAvatar;
  final String name;
  final String time;
  final String title;
  final String comment;

  const CommentWidget(
      {Key? key,
      required this.urlAvatar,
      required this.name,
      required this.time,
      required this.title,
      required this.comment})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: HexColor.fromHex('F3F4F8')),
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
      margin: const EdgeInsets.only(bottom: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Image.asset(urlAvatar),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      name,
                      maxLines: 1,
                      style: TextStyle(
                          color: HexColor.fromHex('1F2124'),
                          fontWeight: FontWeight.w600,
                          fontSize: 16),
                    ),
                    Row(
                      children: [
                        Text(
                          time,
                          style: TextStyle(
                              fontSize: 12,
                              color: HexColor.fromHex('777E89'),
                              fontWeight: FontWeight.w400),
                        ),
                        SvgPicture.asset(Assets.assetsDot),
                        SvgPicture.asset(Assets.assetsEarth),
                      ],
                    )
                  ],
                ),
              ),
              SvgPicture.asset(Assets.assetsMore),
            ],
          ),
          const SizedBox(
            height: 13,
          ),
          Text(
            title,
            maxLines: 1,
            style: TextStyle(
              color: HexColor.fromHex('1F2124'),
              fontWeight: FontWeight.w600,
              fontSize: 14,
            ),
          ),
          Text(
            comment,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: HexColor.fromHex('1F2124'),
              fontWeight: FontWeight.w400,
              fontSize: 14,
            ),
          ),
        ],
      ),
    );
  }
}
