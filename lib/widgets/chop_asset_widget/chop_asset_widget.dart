import 'package:flutter/material.dart';
import 'package:vndirect/extension/color_extensions.dart';
import 'package:vndirect/generated/assets.dart';
import 'package:vndirect/widgets/component_widget/item_invester_widget.dart';
import 'package:vndirect/widgets/component_widget/title_invester_widget.dart';
class ChopAssetWidget extends StatelessWidget {
  const ChopAssetWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 24,),
          _investmentGrow(),
          const SizedBox(height: 18,),
          _investInAssets(),
          const SizedBox(height: 18,),
          _defensiveFinance(),
        ],
      ),
    );
  }


  _investmentGrow(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Tháp tài sản', style: TextStyle(
            fontSize: 20,
            color: HexColor.fromHex('1F2124'),
            fontWeight: FontWeight.w600
        ),),
        const SizedBox(height: 18,),
        const TitleInvestmentWidget(investment: TypeInvestment.investmentGrowth, balance: '500,000,000',),
        const SizedBox(height: 8,),
        SizedBox(
          height: 80,
          child: ListView.builder(
              padding: const EdgeInsets.symmetric(vertical: 8,horizontal: 3),
              scrollDirection: Axis.horizontal,
              itemCount: 2,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return const ItemInverterWidget(
                  listItem: [Assets.assetsBook],
                  asset: Assets.assetsUpToMoon,
                  title: 'Cổ phiếu',
                  content: 'Đầu tư vào doanh nghiệp',);
              }
          ),
        )
      ],
    );
  }

  _investInAssets(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const TitleInvestmentWidget(investment: TypeInvestment.investInAssets, balance: '90,000,000',),
        const SizedBox(height: 8,),
        SizedBox(
          height: 80,
          child: ListView.builder(
              padding: const EdgeInsets.symmetric(vertical: 8,horizontal: 3),
              scrollDirection: Axis.horizontal,
              itemCount: 2,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return const ItemInverterWidget(
                  listItem: [Assets.assetsBook],
                  asset: Assets.assetsUpToMoon,
                  title: 'Cổ phiếu',
                  content: 'Đầu tư vào doanh nghiệp',);
              }
          ),
        )
      ],
    );
  }

  _defensiveFinance(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const TitleInvestmentWidget(investment: TypeInvestment.defensiveFinance, balance: '0',),
        const SizedBox(height: 8,),
        SizedBox(
          height: 80,
          child: ListView.builder(
              padding: const EdgeInsets.symmetric(vertical: 8,horizontal: 3),
              scrollDirection: Axis.horizontal,
              itemCount: 2,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) {
                return const ItemInverterWidget(
                  listItem: [Assets.assetsBook],
                  asset: Assets.assetsUpToMoon,
                  title: 'Cổ phiếu',
                  content: 'Đầu tư vào doanh nghiệp',);
              }
          ),
        )
      ],
    );
  }
}
