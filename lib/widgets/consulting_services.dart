import 'package:flutter/material.dart';
import 'package:vndirect/extension/color_extensions.dart';
import 'package:vndirect/generated/assets.dart';
import 'package:vndirect/widgets/component_widget/title_content_widget.dart';

class ConsultingServices extends StatelessWidget {
  const ConsultingServices({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Dịch vụ tư vấn', style: TextStyle(
              fontSize: 20,
              color: HexColor.fromHex('1F2124'),
              fontWeight: FontWeight.w600
          ),),
          const SizedBox(height: 12,),
          TitleContentWidget(
            colorBg: HexColor.fromHex('F3F4F8'),
            colorCi: HexColor.fromHex('E7E8E9'),
            url: Assets.assetsPhone,
            title: 'Dịch vụ chăm sóc khách hàng',
            content: 'Chuyên viên chăm sóc hỗ trợ bạn mọi lúc mọi nơi',
          ),
          const SizedBox(height: 12,),
          TitleContentWidget(
            colorBg: HexColor.fromHex('F3F4F8'),
            colorCi: HexColor.fromHex('E7E8E9'),
            url: Assets.assetsPhone,
            title: 'Chuyên gia tư vấn',
            content: 'Chuyên gia tư vấn phân bổ và quản lý tài sản',
          ),
        ],
      ),
    );
  }
}
