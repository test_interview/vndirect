import 'package:flutter/material.dart';
import 'package:vndirect/extension/color_extensions.dart';
import 'package:vndirect/generated/assets.dart';
import 'package:vndirect/widgets/component_widget/slider_custom_widget.dart';
import 'package:vndirect/widgets/component_widget/title_content_widget.dart';

class KnowLedgeBaseWidget extends StatelessWidget {
  const KnowLedgeBaseWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Kiến thức nền tảng',
            style: TextStyle(
                fontWeight: FontWeight.w600,
                color: HexColor.fromHex('1F2124'),
                fontSize: 20),
          ),
          const SizedBox(
            height: 12,
          ),
          TitleContentWidget(
            colorBg: HexColor.fromHex('FBECCE'),
            colorCi: HexColor.fromHex('F7DDA8'),
            url: Assets.assetsHappy,
            title: 'Tổng quan về tài chính cá nhân',
          ),
          const SizedBox(
            height: 24,
          ),
          SliderCustom(
            images: [Assets.assetsBanner,Assets.assetsBanner],
            dotHeight: 7,
            dotWidth: 7,
            radius: 15,
            spacing: 6,
            strokeWidth: 1,
            paintingStyle: PaintingStyle.fill,
            dotColor: HexColor.fromHex('E7E8E9'),
            activeDotColor: HexColor.fromHex('745BEF'),
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            onIndexTap: (index) async {},
          )
        ],
      ),
    );
  }
}
