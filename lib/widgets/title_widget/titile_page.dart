import 'package:flutter/material.dart';
import 'package:vndirect/extension/color_extensions.dart';
import 'package:vndirect/generated/assets.dart';
import 'package:vndirect/widgets/title_widget/balance_widget.dart';

class TitlePage extends StatelessWidget {
  const TitlePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return SafeArea(
      bottom: false,
      child: Container(
        padding: const EdgeInsets.only(left: 16, right: 16, top: 8, bottom: 0),
        child: Column(
          children: [
            _title(),
            const SizedBox(
              height: 15,
            ),
            _avatar(),
            const SizedBox(
              height: 15,
            ),
            const BalanceWidget(),
          ],
        ),
      ),
    );
  }

  _title() {

    return const Row(
      children: [
        Icon(Icons.menu, color: Colors.white, size: 24),
        Spacer(),
        Icon(Icons.headset_mic_outlined, color: Colors.white, size: 24),
        SizedBox(
          width: 16,
        ),
        Icon(Icons.notifications_none, color: Colors.white, size: 24),
      ],
    );
  }

  _avatar() {

    return Row(
      children: [
        Expanded(
            child: Image.asset(
          Assets.assetsAvatar,
        )),
        Expanded(
            flex: 7,
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Xin chao buoi sang',
                    maxLines: 1,
                    style: TextStyle(
                      color: HexColor.fromHex('#EFECFF'),
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  const Text(
                    'Nguyễn Thị Thùy Linh',
                    maxLines: 1,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ))
      ],
    );
  }
}
