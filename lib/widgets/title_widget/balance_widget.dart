import 'package:flutter/material.dart';
import 'package:vndirect/extension/color_extensions.dart';
import 'package:vndirect/generated/assets.dart';

class BalanceWidget extends StatelessWidget {
  const BalanceWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10,vertical: 7),
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage(Assets.assetsMask), // Replace with your image path
          fit: BoxFit.cover, // You can adjust the BoxFit as needed
        ),
      ),
      child: Row(
        children: [
          Image.asset(Assets.assetsChop),
          Expanded(child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Tài sản ròng',  maxLines: 1, style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 12,
                color: HexColor.fromHex('#353A40')
              ),),
              Text(
                  '800,000,000đ',
                  maxLines: 1,
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 24,
                      color: HexColor.fromHex('#353A40')
                  )
              ),
            ],
          )),
          Icon(Icons.arrow_forward_ios_sharp,size: 24,color:  HexColor.fromHex('#5031EB'),)
        ],
      ),

    );
  }
}
