# VNDirect

## how to run project

```
flutter pub get
flutter run
```

## Interview lesson algorithm

```
void main() {
  final solution = Solution();
  //lesson2
  final list1 = [1, 2, 3, 4, 6, 5];
  final list2 = [5, 6, 8, 9, 5];
  
  final equal1 = solution.revertNumberList(list1,2);
  final equal2 = solution.revertNumberList(list2,3);
  
  print('===========test2.1 $equal1');
  print('===========test2.2 $equal2');
  
   //lesson3
  final list31 = [0,0,2,1,1];
  final list32 = [10,2,3,4,5,7,8];
  
  final equal31 = solution.findSumQuanlity(list31, 3);
  final equal32 = solution.findSumQuanlity(list32, 23);
  
  print('===========3.1 $equal31');
  print('===========3.2 $equal32');
 
}

class Solution {
  
  //------lesson-2--------
  List<int> revertNumberList(List<int> arr,int k) {
    if(arr.isEmpty) return [];
    
    if(k == 0) return arr;
    
    final n = arr.length;
    
    if(k >= n) return arr.reversed.toList();
    
    
     List<int> listEqual = [];
    
    for (int i = 0; i < n; i += k) {
        final endIndex = (i + k < n) ? i + k : n;
        listEqual = listEqual + arr.sublist(i, endIndex).reversed.toList();
    }
    
    
    return listEqual;
  }
  
  
  //------lesson-3--------
  List<List<int>> findSumQuanlity(List<int> arr, int number){
    List<List<int>> result = [];
    final n = arr.length;
  
    if (n < 4) {
      return result;
    }
  
    arr.sort();
  
   for (int i = 0; i < n - 3; i++) {
    for (int j = i + 1; j < n - 2; j++) {
      for(int k = j + 1; k < n -1; k++){
        for(int l = k + 1; l < n ; l++){
   
          if((arr[i] + arr[j] + arr[k] + arr[l]) == number){
            result.add([arr[i],arr[j],arr[k],arr[l]]);
          }
          
          if((arr[i] + arr[j] + arr[k] + arr[l]) > number){
            break ;
          }
        }
      }
    }
   }
    
  // ---- remote item duplicate -------
    
    if(result.isEmpty){
      return [];
    }
    
   final resultData = [result[0]];


  for (int i = 1; i < result.length; i++) {
    if (!areListsEqual(result[i], result[i - 1])) {
      resultData.add(result[i]);
    }
  }

  return resultData;
  }
  
  bool areListsEqual(List<int> list1, List<int> list2) {
  if (list1.length != list2.length) {
    return false;
  }

  for (int i = 0; i < list1.length; i++) {
    if (list1[i] != list2[i]) {
      return false;
    }
  }

  return true;
  }

 
}

```

